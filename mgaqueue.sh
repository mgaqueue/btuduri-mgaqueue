#!/bin/bash
#
#	Mgaqueue
#
#	Usage: mgaqueue pkg1 pkg2 ... OR mgaqueue -file list_pkgs.txt
#	Usage: list_pkgs must have one pkgs per line
#	needs curl to work
#
#
# Copyright 2011
#	Julien Moragny <julien.moragny _at_ laposte.net>
#	John Balcaen <mikala _at_ littleboboy.net>
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author, the holder of the
# economic rights, and the successive licensors  have only  limited
# liability. 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

#set -e
#set -x

# Delay in seconds between check of package
DELAY=30

unset ARG1
unset ARG2
unset ARGS
unset queue
unset status
unset BUILDSTATUS
unset ligne


ARGS=$@
ARG1=$1
ARG2=$2

change_delay() {
	local thresold="$1"
	
	if [ ! -z "$thresold" ];
	then
		DELAY="$1"
	fi
}

print_message() {
	local message="$1"

	if [ ! -z "$message" ];
        then
		echo -e "$message"
	fi
	
}

print_error() {
	local message="$1" 
	print_message "$message"
	exit 1;
}

test_for_curl() {
	which HEAD >/dev/null 2>&1
	if [ "$?" = "1" ]; then
		print_error "\033[1mWarning:\033[0m  HEAD not found\n
                             \033[1mWarning:\033[0m please install perl-libwww-perl"
	fi
}

create_temporary_file() {
queue=`mktemp` && status=`mktemp` || exit 1
if [ $? -ne 0 ];
 	then
		print_error "\033[1m\033[31m Error:\033[0m Unable to create temporary file. Aborting."
fi
}

test_for_arguments() {
if [ "$ARG1" = "-file" ]; then
		if [ ! -f "$ARG2" ];then
			print_error "$ARG2: No such file or not a regular file"
		fi
		cp -f "$ARG2" "$queue"
		
else
		echo $ARGS |tr " " "\n"> $queue
fi
}

clean_temp() {
rm -f "$queue"
rm -f "$status"
unset ARGS
unset ARG1
unset ARG2
unset queue
unset ligne
}

wait_for_build() {
until [ "$BUILDSTATUS" = " uploaded" ]; do
	sleep $DELAY
	BUILDSTATUS=`HEAD 'http://pkgsubmit.mageia.org/index2.php?package='$ligne'&last=1' |grep X-BS-Package-Status|cut -d : -f 2`
		if [ "$BUILDSTATUS" = " failure" ]; then
			print_error "Build has failed for $ligne, exiting"
		elif [ "$BUILDSTATUS" = " rejected" ]; then
			print_error "Build was rejected for $ligne"
		elif [ "$BUILDSTATUS" = " building" ]; then
			if [ "$etatbuild" != "$BUILDSTATUS" ]; then
			      print_message "Status: Building"
			      etatbuild="$BUILDSTATUS"
			fi
			change_delay 180
		elif [ "$BUILDSTATUS" = " partial" ]; then
			if [ "$etatbuild" != "$BUILDSTATUS" ]; then
			      print_message "Status: Partial"
			      etatbuild="$BUILDSTATUS"
			fi
			change_delay 90
		elif [ "$BUILDSTATUS" = " todo" ]; then
			if [ "$etatbuild" != "$BUILDSTATUS" ]; then
			      print_message "Status: To do"
			      etatbuild="$BUILDSTATUS"
			fi
			change_delay 240
		elif [ "$BUILDSTATUS" = " uploaded" ]; then
			echo "Status: Uploaded, go to next package"
			change_delay 15
			unset etatbuild
		fi
done
}

if [ "$#" -lt 1 ]; then
        print_error "\n \033[1m     Mgaqueue\033[0m \n 
      Usage: mgaqueue pkg1 pkg2 ... OR mgaqueue -file list_pkgs.txt \n
      Usage: list_pkgs must have one pkgs per line \n
      needs perl-libwww-perl to work \n"
fi

create_temporary_file
test_for_curl
test_for_arguments

for ligne in $(cat $queue)
do
	THROTTLE=`HEAD 'http://pkgsubmit.mageia.org/'|grep Throttle |cut -d : -f 2`
	print_message "Processing $ligne in $THROTTLE seconds"
	sleep $THROTTLE
	mgarepo submit "$ligne"
	wait_for_build
	unset BUILDSTATUS
done 
clean_temp
print_message "Jobs finished"
